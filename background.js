chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  let content = "from the extension";
  console.log(sender.tab ? "from a content script:" + sender.tab.url : content);
  chrome.tabs.create({
    url: `https://translate.google.com/?sl=en&tl=vi&text=${request.message}&op=translate`,
  });
  sendResponse({ message: "OK" });
});
