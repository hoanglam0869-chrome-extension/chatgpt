document.onkeyup = (e) => {
  let msgBox = document.querySelector("#prompt-textarea");
  if (e.key === "ArrowLeft") {
    let content = document.querySelector(".markdown.prose.w-full.break-words");
    msgBox.value = content.innerText.length;
  } else if (e.key === "ArrowRight") {
    let btns = document.querySelectorAll(
      ".p-1.rounded-md.text-token-text-tertiary"
    );
    btns[2].click();
  } else if (e.key === "ArrowUp") {
    let content = document.querySelector(".markdown.prose.w-full.break-words");
    sendMessage(content.innerText.replaceAll("\n", "%0A"));
  }
};

async function sendMessage(message) {
  const response = await chrome.runtime.sendMessage({ message });
  // do something with response here, not outside the function
  console.log(response);
}
